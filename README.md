用于web服务器搭建，内含各种脚本，安装过程如下

第一、准备工作

这里我用centos 6 64环境，其实这个脚本是支持CentOS 5-7、Debian 6-8、Ubuntu 12-15环境版本的。我们可以根据自己的喜欢和习惯安装。

第二、部署脚本设置环境

yum -y install wget screen python
wget http://mirrors.linuxeye.com/oneinstack-full.tar.gz
tar xzf oneinstack-full.tar.gz
cd oneinstack
screen -S oneinstack
./install.sh

直接登录我们的VPS主机SSH，然后丢上上面的环境，然后回车后自动下载。

第三、部署JAVA环境过程记录

1、基本环境选择

基本环境选择

我们根据自己的需要设置新的ROOT端口，安全提高一些，这里我是乱写的几个数字，根据我们自己的实际写几个不同的数字，反正不用22就可以。然后选择NGINX、TOMCAT、JDK等环境版本。

2、设置数据库和其他配置

3、其他的配置组件

其他的配置组件

根据实际需要安装需要的组件和缓存功能，这里我就大概选择。然后回车就可以自动安装。


第四、常见OneinStack用户命令

1、如何添加虚拟主机

./vhost.sh

2、如何删除虚拟主机

./vhost.sh del

3、如何管理FTP账号

./pureftpd_vhost.sh

4、如何备份

A 备份设置

./backup_setup.sh

B 备份执行

./backup.sh # Start backup, You can add cron jobs
# crontab -l
0 1 * * * cd ~/oneinstack;./backup.sh  > /dev/null 2>&1 &
## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).